import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { BorderCardDirective } from "../shared/directives/border-card.directive";
import { PizzaIngredientColorPipe } from "../shared/pipes/pizza-ingredient-color.pipe";
import { DetailsPizzaComponent } from "./details-pizza/details-pizza.component";
import { ListPizzaComponent } from "./list-pizza/list-pizza.component";
import { PizzasService } from "./pizzas.service";
import { PizzaFormComponent } from './pizza-form/pizza-form.component';
import { FormsModule } from "@angular/forms";






@NgModule({
    declarations: [
      ListPizzaComponent,
      BorderCardDirective,
      PizzaIngredientColorPipe,
      DetailsPizzaComponent,
      PizzaFormComponent,
      ],
    imports: [
      CommonModule,
      FormsModule
    ],
    providers: [
      PizzasService,
    ],
  })

  export class PizzasModule { }