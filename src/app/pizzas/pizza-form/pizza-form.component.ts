import { Component, Input, OnInit } from '@angular/core';
import { Pizza } from 'src/app/Pizza';
import { Router } from '@angular/router';
import { PizzasService } from '../pizzas.service';

@Component({
  selector: 'app-pizza-form',
  templateUrl: './pizza-form.component.html',
  styleUrls: ['./pizza-form.component.scss']
})
export class PizzaFormComponent implements OnInit {

  ingredients : Array<string> | undefined;
  @Input() pizza: Pizza | undefined; 

  constructor(private router: Router, private pizzasService: PizzasService) { }

  ngOnInit(): void {
    this.ingredients = this.pizzasService.getPizzaIngredients();
  }

  hasIngredient(type: string): boolean  {
    const index = this.pizza?.compositions?.indexOf(type);
    return (index !== -1 ) ? true : false;
  }

  selectIngredient($event: any, ingredient: string): void {
    const checked = $event.target.checked;
    if(checked) {
      this.pizza?.compositions?.push(ingredient);

    } else {
      const index = this.pizza?.compositions?.indexOf(ingredient);
      if(index && index > -1) {
        this.pizza?.compositions?.splice(index, 1);
      }
    }
  }

  onSubmit(): void {
    console.log("Submit Form ");
    const link = ['/pizza', this.pizza?.id];
    this.router.navigate(link)
  }
}
