import { Component, OnInit } from '@angular/core';
import { Pizza } from 'src/app/Pizza';
import { ActivatedRoute, Router } from '@angular/router';
import { PizzasService } from '../pizzas.service';


@Component({
  selector: 'app-details-pizza',
  templateUrl: './details-pizza.component.html',
  styleUrls: ['./details-pizza.component.scss']
})
export class DetailsPizzaComponent implements OnInit {
  pizzaToDisplay : Pizza | undefined;
  listOfPizzas : Pizza[] | undefined;

  constructor(private route: ActivatedRoute, private router: Router, private pizzasService: PizzasService) {}

  ngOnInit(): void {
        // Récupérer le paramètre de la route associée à notre composant
    // Snapshot : Récupérer le paramètre de manière asynchrone
    const retrievedIdFromURL = +this.route.snapshot.params['id'];

    this.listOfPizzas = this.pizzasService.getListPizzas();
    // Trouver la pizza dont l'identifiant correspond à celui
    // récupéré depuis l'URL
    this.pizzaToDisplay = this.pizzasService.getSinglePizza(retrievedIdFromURL);

    console.log('Pizza Selectionnée : ', this.pizzaToDisplay);

  }

  goBack() {
    this.router.navigate(['/pizzas']);
  }
  
}
