import { Injectable } from '@angular/core';
import { Pizza } from '../Pizza';
import { LIST_PIZZAS } from '../shared/list.pizza';

@Injectable({
  providedIn: 'root'
})
export class PizzasService {

  constructor() { }

  /* Récuperer la liste des pizzas*/

  getListPizzas(): Pizza[] {
    return LIST_PIZZAS;
  }


  /**  
  * Retourner la pizza avec l'identifiant passé en paramètre
  * @param id
  */
  getSinglePizza(id: number) : Pizza | undefined {
    const listPizzas = this.getListPizzas();
    return listPizzas.find(pizza => pizza.id === id);
  }

  /** Récupere les type d'une pizza */
  getPizzaIngredients(): string[] { 
    return ['S. tomate', 'v. kebab', 'roquette', 'piments','miel', 'C. fraîche', 'v. hachée', 'S.barbecue', 'champignons',
    'merguez', 'mozzarella', 'oignons'];
  }
}
