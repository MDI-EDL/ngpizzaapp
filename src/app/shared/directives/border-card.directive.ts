import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appPizzaBorderCard]'
})
export class BorderCardDirective {

  @Input('appPizzaBorderCard') borderColor: string | undefined;
  GREY_COLOR = '#f5f5F5';
  GREEN_COLOR = '#009688';
  PINK_COLOR = '#ee6e73';

  constructor(private element: ElementRef) { 
    this.setBorder(this.PINK_COLOR);
    this.setHeight(180)
  }

  /**
   * Definir une bordure
   * @param color 
   * @private
   */
  private setBorder(color: string) : void {
    const border = 'solid 4px ' + color;
    this.element.nativeElement.style.border = border;
  }

  /**
   * Definir une hauteur
   * @param number 
   * @private
   */
  private setHeight(height: number) : void {
    this.element.nativeElement.style.height = height + 'px';
  }

  /**
   * Création d'un évènement qui change la couleur 
   */
   @HostListener('mouseenter') onMouseEnter() {
    //this.setBorder(this.GREEN_COLOR);
    this.setBorder(this.borderColor || this.GREEN_COLOR);
  }

   /**
   * Création d'un évènement qui change la couleur dès que l'on quitte l'element
   */
    @HostListener('mouseleave') onMouseLeave() {
      this.setBorder(this.PINK_COLOR);
    }
}
