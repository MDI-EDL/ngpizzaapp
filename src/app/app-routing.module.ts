import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailsPizzaComponent } from './pizzas/details-pizza/details-pizza.component';
import { ListPizzaComponent } from './pizzas/list-pizza/list-pizza.component';
import { PageNotFoundComponent } from './shared/page-not-found/page-not-found.component';


const routes: Routes = [
  { path: 'pizzas', component: ListPizzaComponent},
  { path: 'pizzas/:id', component: DetailsPizzaComponent},
  {path: '', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
